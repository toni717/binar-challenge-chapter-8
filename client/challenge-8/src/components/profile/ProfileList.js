import React, { useContext } from "react";
import PageContext from "../../EditOrCreateContext";
import ProfileItem from "./profileItem";

function ProfileList () {
    const {allProfile, removeProfile, handleEdit} = useContext(PageContext);
    
    return (
        <React.Fragment>
            {allProfile && allProfile.map( (profile) => (
                <ProfileItem 
                    key = {profile.email}
                    username ={profile.username}    
                    email = {profile.email}
                    experience ={profile.experience}
                    level = {profile.level}
                    onDelete = {() => {removeProfile (profile.email)}}
                    onUpdate ={() => {handleEdit(profile.email)}}
                />
            ))}
        </React.Fragment>
    );
}

export default ProfileList;