import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

function ProfileItem (props) {
    return (
        <div className="bg-light m-3 w-25" style={{minWidth:300}}>
        <div className="card-header"><strong>{props.username}</strong></div>
        <div className="mx-3 text-muted">{props.email}</div>

        <div className="w-100 px-3">
            <table className="table-borderless w-100 my-3">
                <tbody>
                    <tr>
                        <td className="py-2"> <u> Experience </u></td>
                        <td className="py-2 text-secondary"> {props.experience}</td>
                    </tr>
                    <tr className="">
                        <td className="py-2 text-underline"> <u> Level </u></td>
                        <td className="py-2 text-secondary"> {props.level}</td>
                    </tr>
                </tbody>
            </table>
            <Link to="/editorcreate"><button onClick={props.onUpdate} className="mx-1 mb-3 btn btn-secondary">Edit</button></Link>
            <button onClick={props.onDelete} className="mx-1 mb-3 btn btn-warning">Delete</button>
        </div>      
    </div>
    );
}

export default ProfileItem;