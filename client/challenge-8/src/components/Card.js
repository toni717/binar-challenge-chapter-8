import 'bootstrap/dist/css/bootstrap.min.css';

function Card ({children}) {
    return(
    <div className = "card w-50 m-5 p-3 bg-light">
        {children}
    </div>
    );
}

export default Card;