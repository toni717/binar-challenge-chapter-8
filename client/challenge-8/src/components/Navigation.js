import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useContext} from 'react';
import {Link, Redirect, useLocation} from 'react-router-dom';
import PageContext from '../EditOrCreateContext';

function Navigation() {
    let location = useLocation();
    const {page, handlePage, allProfile} = useContext(PageContext);
    
    const handleChange = (e) => {  
        handlePage(e.target.getAttribute('name'));
    }
   
    return (
        <nav className="navbar navbar-dark bg-dark  d-flex py-2 justify-content-center align-items-center container-fluid"> 
            { location.pathname === '/'  ? 
            <div className="container d-flex justify-content-center">
            <Link to="/editorcreate" name="edit" className="text-decoration-none h1 navbar-brand" > 
                <span name="Create Profile" onClick={handleChange} >Create Profile </span>
            </Link>
            {allProfile.length && <Link to="/editorcreate" name="edit" className="text-muted text-decoration-none"> 
            <span name="Find Profile" onClick={handleChange} >Find </span>
            </Link> }
            </div> :
            (page ? <span className="navbar-brand h1"> {page}</span> : <Redirect to="/"></Redirect>)
            }
        </nav>        
    );
}

export default Navigation;