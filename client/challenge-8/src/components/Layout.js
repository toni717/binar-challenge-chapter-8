import Navigation from "./Navigation";

function Layout({children}) {
    return (
        <div>
            <Navigation />
            <div className="d-flex justify-content-center row">
                {children}
            </div>
        </div>
    );
}

export default Layout;