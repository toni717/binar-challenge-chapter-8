import 'bootstrap/dist/css/bootstrap.min.css';
import { useContext, useRef, useState } from 'react';
import Card from './Card';
import PageContext from '../EditOrCreateContext';
import { Link, useHistory } from 'react-router-dom';
import ProfileItem from './profile/profileItem';


function Form () {
  let history = useHistory();
  let profile={};
  const [errorEmail, setErrorEmail] = useState(false);
  const [{errorFind, showCard}, setErrorFind] = useState({errorFind : false, showCard:false});
  const {page, enterProfile, allProfile, editedProfile, removeProfile, clearEditedProfile, findProfile, handleEdit} = useContext(PageContext);
  const username= useRef();
  const email = useRef();
  const experience = useRef();
  const level = useRef();

  const handleSubmit = (event) => {
    event.preventDefault();
    profile = {
      username : username.current.value,
      email : page !=="Edit Profile" ? email.current.value : null,
      experience : experience.current.value,
      level : level.current.value,
    }
    if(page === "Create Profile") {
      if(allProfile.some(oldprofile => oldprofile.email === profile.email )) {
        setErrorEmail(true);
      } else {
        setErrorEmail(false);
        enterProfile(profile)
        history.replace('/');
      }
    } else if(page === "Edit Profile") {
        let currentEmail = editedProfile.email
        removeProfile(currentEmail);
        clearEditedProfile();
        enterProfile({...profile, email : currentEmail});
    } else {
      let findedProfile = findProfile(profile.email);
      if(findedProfile) {
        if(JSON.stringify(findedProfile) !== JSON.stringify(profile)) {
          setErrorFind({errorFind:true, showCard:false});
        } else {
          setErrorFind({errorFind:false, showCard:true});
        }
      } else {
        setErrorFind({errorFind:false, showCard:true});
      }
    }     
  }
  return (
  <Card>
  {errorEmail && <div className =" p-3 bg-success text-white"> Account Exist </div>}
  {(errorFind && page === 'Find Profile') && <div className =" p-3 bg-danger text-white"> Account not Found </div>}
  {(showCard && page === 'Find Profile') && <ProfileItem 
                  key = {email.current.value}
                  username ={username.current.value}    
                  email = {email.current.value}
                  experience ={experience.current.value}
                  level = {level.current.value}
                  onDelete = {() => {removeProfile (email.current.value)}}
                  onUpdate ={() => {handleEdit(email.current.value)}}
              />}
  
  <form onSubmit={handleSubmit}>
      <div className="form-group m-3">
        <label htmlFor="userName" className="col-form-label">User Name</label>
        <input ref={username} type="text" className="form-control" id="userName" required/>
      </div>
      {page !== "Edit Profile" &&
        <div className="form-group m-3">
          <label htmlFor="email" className=" col-form-label">Email</label>
          <input ref={email} type="email" className="form-control" id="email" required/>
        </div>
      }
      <div className="form-group m-3">
        <label htmlFor="experience" className="col-form-label">Experience</label>
        <select  ref={experience} type="text" className="form-control" id="experience" required >
          <option>Beginner</option>
          <option>Preintermediate</option>
          <option>Intermediate</option>
          <option>Advanced</option>
        </select>
      </div>
      <div className="htmlForm-group m-3">
        <label htmlFor="level" className="col-form-label">Level</label>
        <input ref={level} type="number" className="form-control" id="level" required/>
      </div>
      <button type="submit" className="btn btn-primary m-3">Submit</button>
      <Link to='/'><button type="button" className="btn btn-secondary">All Profiles</button></Link>
  </form>
  </Card>
  );
}

export default Form;