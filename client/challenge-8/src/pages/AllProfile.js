import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ProfileList from '../components/profile/ProfileList';

function AllProfile () {    
    return (
        <React.Fragment>
            <ProfileList />
        </React.Fragment>
    );  
}

export default AllProfile;