import 'bootstrap/dist/css/bootstrap.min.css';
import {Route, Switch} from 'react-router-dom';
import AllProfile from './pages/AllProfile';
import Layout from './components/Layout';
import EditOrCreate from './pages/EditOrCreate';

function App() {
  return (  
    <Layout>
      <Switch>
        <Route path='/' exact>
          <AllProfile />
        </Route>
        <Route path='/editorcreate'>
          <EditOrCreate />
        </Route>
      </Switch>
    </Layout>
  );
}

export default App;
