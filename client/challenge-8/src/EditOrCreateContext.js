import { createContext, useEffect, useState } from "react";

const PageContext = createContext ({
    page : "", 
    handlePage : (name) => {},
    allProfile :[],
    enterProfiles :(profile) => {},
    removeProfile : (profile) => {},
    handleEdit : (email) => {},
    editedProfile : {},
    clearEditedProfile : () => {},
    findProfile : (email) => {}
});

export function EditOrDeleteContext ({children}) {
    const [page, setPage] = useState("");
    const [allProfile, setProfile] = useState([]);
    const [editedProfile, setEditedProfile] = useState({});

    function handlePage(name) {
        setPage(name);
    }

    function enterProfile(profile) {
        setProfile((allProfile) => { return [profile, ...allProfile]})
    }

    function removeProfile(removedEmail) {
        setProfile( profiles => {
            profiles = profiles.filter(profile => profile.email !== removedEmail)
            return [...profiles]
        })
    }

    function handleEdit(email) {
        setPage("Edit Profile")
        let profile = allProfile.find(profile => profile.email === email);
        setEditedProfile(profile);
    }

    function clearEditedProfile () {
        setEditedProfile( prev => prev ={});
    }

    function findProfile (email) {
        let profile = allProfile.find(profile => profile.email === email)
        return profile;
    }

    let context = {
        page, 
        handlePage,
        allProfile,
        enterProfile,
        removeProfile,
        handleEdit,
        editedProfile,
        clearEditedProfile,
        findProfile,
    }

    useEffect( () => {
        if(allProfile) {
            setPage("");
        }
      },[allProfile])
  
    return (
        <PageContext.Provider value={context}>
            {children}
        </PageContext.Provider>
    );
}

export default PageContext